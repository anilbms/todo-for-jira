# Todo Forge Jira issue panel app

This project contains a Forge app written in Javascript that displays options to **add todo item** and **todo list** in a Jira issue panel.

## Store Api

- This app uses **Store Api** called [onJiraProject](https://developer.atlassian.com/platform/forge/runtime-api-reference/store-api/#onjiraproject) to **store** to do items a key-value pair on a specified Jira project.

## Quick demo

[![Todo Forge Jira issue panel app demo](http://img.youtube.com/vi/yrRjLxnlDGM/0.jpg)](https://youtu.be/yrRjLxnlDGM)

## Quick start

- Install dependencies by running:

  ```
  npm install
  ```

- Build and deploy your app by running:

  ```
  forge deploy
  ```

- Install your app in an Atlassian site by running:

  ```
  forge install
  ```

### Notes

- Deploy your app, with the `forge deploy` command, any time you make changes to the code.
- Install your app, with the `forge install` command, when you want to install your app on a new site. Once the app is installed on a site, the site picks up the new app changes you deploy without needing to run the install command again.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
If you have any questions or feedback, contact [Anil Kumar Krishnashetty](mailto:anilbms75@gmail.com) or on Twitter [@anilbms75](https://twitter.com/anilbms75).


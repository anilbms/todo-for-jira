import ForgeUI, {
    useProductContext,
    useState,
} from '@forge/ui';
import api from '@forge/api';

export default () => {
    const context = useProductContext();
    const {
        accountId,
        platformContext: {
            issueKey
        },
    } = context;

    const [todos, setTodos] = useState(async () => (await api.store.onJiraIssue(issueKey).get('todos') || []));

    return {
        todos,
        addTodo: async (todoText) => {
            const newToDo = {
                name: todoText,
                accountId: accountId,
                isItDone: false
            };
            setTodos([...todos, newToDo]);
            await api.store.onJiraIssue(issueKey).set('todos', [...todos, newToDo]);
        },
        deleteTodo: async (todoIndex) => {
            const newTodos = await todos.filter((_, index) => index !== todoIndex);
            setTodos(newTodos);
            await api.store.onJiraIssue(issueKey).set('todos', newTodos);
        },
        updateToDoToDone: async (todoIndex) => {
            const newToDos = await todos.map((todo, index) => {
                console.log('INSIDE map -----> index, todoIndex', index, todoIndex);
                if (index === todoIndex) {
                    console.log('INSIDE match');
                    return Object.assign(todo, {
                        isItDone: true
                    });
                } else {
                    console.log('INSIDE not match ---->');
                    return todo;
                }
            });
            setTodos(newToDos);
            await api.store.onJiraIssue(issueKey).set('todos', newToDos);
        },
        updateToDoToNotDone: async (todoIndex) => {
            const newToDos = await todos.map((todo, index) => {
                if (index === todoIndex) {
                    console.log('INSIDE match');
                    return Object.assign(todo, {
                        isItDone: false
                    });
                } else {
                    console.log('INSIDE not match ---->');
                    return todo;
                }
            });
            setTodos(newToDos);
            await api.store.onJiraIssue(issueKey).set('todos', newToDos);
        },
    };
};
import ForgeUI, {
  Fragment,
  useState,
  Form,
  TextField,
  ModalDialog,
  Button,
} from '@forge/ui';

const ToDoForm = ({ saveToDo }) => {
  const [isModalDialogOpen, setIsModalDialogOpen] = useState(false);
  const [value, setValue] = useState('');
  return (
    <Fragment>
      <Button text="ADD TO DO" onClick={() => setIsModalDialogOpen(true)} />
      {isModalDialogOpen && (
        <ModalDialog
          header="Add to do"
          onClose={() => setIsModalDialogOpen(false)}
        >
          <Form
            onSubmit={(data) => {
              saveToDo(data.newTodo);
              setValue('');
              setIsModalDialogOpen(false);
            }}
          >
            <TextField
              name="newTodo"
              label="New to do"
              placeholder="add to do"
              defaultValue={value}
              isRequired={true}
            />
          </Form>
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default ToDoForm;

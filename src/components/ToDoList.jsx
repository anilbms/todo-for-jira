import ForgeUI, {
  Fragment,
  Text,
  Button,
  Table,
  Row,
  Cell,
  Avatar,
  Head,
  ButtonSet,
} from '@forge/ui';

const TodoList = ({
  todos,
  deleteTodo,
  updateToDoToDone,
  updateToDoToNotDone,
}) => (
  <Fragment>
    <Table>
      <Head>
        <Cell>
          <Text content="**Name**" />
        </Cell>
        <Cell>
          <Text content="**Action**" />
        </Cell>
        <Cell>
          <Text content="**Creator**" />
        </Cell>
      </Head>
      {todos.map(({ name, accountId, isItDone }, index) => (
        <Row>
          <Cell>
            <Text>{isItDone ? `✅ ~~${name}~~` : `⏳${name}`}</Text>
          </Cell>
          <Cell>
            <ButtonSet>
              <Button
                text="delete"
                onClick={() => {
                  deleteTodo(index);
                }}
              />
              {!isItDone && (
                <Button
                  text="mark as done"
                  onClick={() => {
                    updateToDoToDone(index);
                  }}
                />
              )}
              {isItDone && (
                <Button
                  text="mark as not done"
                  onClick={() => {
                    updateToDoToNotDone(index);
                  }}
                />
              )}
            </ButtonSet>
          </Cell>
          <Cell>
            <Avatar accountId={accountId} />
          </Cell>
        </Row>
      ))}
    </Table>
  </Fragment>
);

export default TodoList;

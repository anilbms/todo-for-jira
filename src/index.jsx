import ForgeUI, { render, Fragment, IssuePanel } from '@forge/ui';
import ToDoList from './components/ToDoList';
import ToDoForm from './components/ToDoForm';
import useTodoState from './useTodoState';

const App = () => {
  const {
    todos,
    addTodo,
    deleteTodo,
    updateToDoToDone,
    updateToDoToNotDone,
  } = useTodoState();

  return (
    <Fragment>
      <ToDoForm
        saveToDo={(todoText) => {
          const trimmedText = todoText.trim();
          if (trimmedText.length > 0) {
            addTodo(todoText);
          }
        }}
      />
      <ToDoList
        todos={todos}
        deleteTodo={deleteTodo}
        updateToDoToDone={updateToDoToDone}
        updateToDoToNotDone={updateToDoToNotDone}
      />
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
